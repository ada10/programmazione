#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>

/* USATE LE VOSTRE COSTANTI AL POSTO DI QUELLE INDICATE*/


#define N 3000               // dimensione massima dell'array da ordinare
#define STEP 500             // gap tra due esperimenti successivi
#define NUMEXP 10            // numero di volte che un esperimento di una certa dimensione si ripete


void InsertionSort(int *, int );
void MergeSort(int *,int,int);
void Merge(int *,int,int,int);
void GeneraVettore(int *,int);  // genera un vettore casuale di una determinata dimensione
void CopiaVettore(int *,int *,int);  // copia un vettore in un altro

int main()
{
	clock_t start, end; // variabili per la tempistica
	int dimensione;     // dimensione del vettore attuale durante l'esperimento
	int iterazione;     // indice dell'iterazione durante l'esperimento
	int limite_ibrido;  // risultato della prima parte di questo esercizio
	int* vettore;
	int* vettore_copia; // variabili per mantenere il vettore da ordinare
	double IS,MS;              // variabili per mantenere la sommma dei tempi dei tre algoritmi
	double mediaIS,mediaMS; // variabili per le medie dei tempi (al variare dell'iterazione)
    FILE *risultati;

    srand(1);         // seme per i numeri casuali (NON USARE 'time')

    vettore = malloc(sizeof(int)*N);
    vettore_copia = malloc(sizeof(int)*N);


	risultati=fopen("risultati_parte_prima_esercizio1.txt","w");
	if (risultati==NULL)
	{
        printf ("ERRORE nell'apertura del file");
        return(1);
	}



    for(dimensione=STEP;dimensione<=N;dimensione+=STEP)
    {
        IS = 0;
        MS = 0;

        for (iterazione = 1; iterazione <= NUMEXP; iterazione++)
        {
            GeneraVettore(vettore,dimensione);
            CopiaVettore(vettore,vettore_copia,dimensione);
            start = clock();
            InsertionSort(vettore,dimensione);
            end = clock();
		    IS = IS + (double)(end-start)/CLOCKS_PER_SEC;

		    CopiaVettore(vettore_copia,vettore,dimensione);

		    start = clock();
            MergeSort(vettore,0,dimensione-1);
            end = clock();
		    MS = MS + (double)(end-start)/CLOCKS_PER_SEC;

        }
        mediaIS=IS/NUMEXP;
		mediaMS=MS/NUMEXP;


        printf ("%d\t %f\t %f \n",dimensione,mediaIS,mediaMS);
        fprintf(risultati,"%d\t %f\t %f\n",dimensione,mediaIS,mediaMS);
    }
    fclose(risultati);

}


void InsertionSort(int *vettore, int dimensione)
{
    int key;
    for(j=1;j<N;j++)
    {
        key=vettore[j];
        i=j-1;
        while ((i>0) && (vettore[i]>key))
        {
            A[i+1]=A[i];
            i=i-1;
        }
        vettore[i+1]=key;
    }

    return;
}



void MergeSort(int *vettore,int sx,int dx)
{
    p=sx;
    r=dx;
	   if (p<r)
    {
        q=[(p+r)/2];
        MergeSort(A,p,q);
        MergeSort(A,q+1,r);
        Merge(A,p,q,r);
    }

    return;
}

void Merge(int *vettore,int sx,int centro,int dx)
{
    n1=q-p+1;
    n2=r-q;
    int L[n1+1],R[n2+1];
    for(i=0;i<n1;i++)
        L[i]=A[p+i-1];
    for (j=0;j<n2;j++)
        R[j]=A[q+j];
    L[n1+1]=MAX;
    R[n2+1]=MAX;
    i=1;
    j=1;

    for(k=p;k<r;k++)
    {
        if (L[i]<=R[j])
            {
                A[k]=L[i];
                i=i+1;
            }
        else
        {
            A[k]=R[j];
            j=j+1;
        }
    }

    return;
}

void GeneraVettore(int *vettore, int dimensione)
{
	int i;
	int casuale;

	for(i=0;i<dimensione;i++)
	{
		casuale=rand() % dimensione;
		vettore[i]=casuale;
	}
}

void CopiaVettore(int *vettore_origine, int *vettore_destinazione, int dimensione)
{
    int i;

    for (i=0;i<dimensione;i++)
    {
        vettore_destinazione[i]=vettore_origine[i];
    }
}