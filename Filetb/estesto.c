#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DIM 200

int main(void)
{
    FILE *f;
    char p[DIM];

    f = fopen("estesto.txt", "wt");
    if (f == NULL)
    {
        printf("file esempio.txt non trovato");
        exit(-1);
    }

    while (strcmp(p, "FINE") != 0)
    {
        printf("scrivere una parola (per terminare digitare FINE):\n");
        scanf("%s", p);
        fprintf(f, "%s\n", p);
    }

    fclose(f);
}