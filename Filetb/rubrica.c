#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DIM 20
#define DIM1 30
#define DIM2 15

typedef struct
{
    char nome[DIM];
    char indirizzo[DIM1];
    char numero[DIM2];
} Persona;

int main(void)
{
    Persona per;
    FILE *f;
    char p[DIM];
    int i=0;
    f = fopen("rubrica.txt", "rt");
    if (f == NULL)
    {
        printf("rubrica.txt non trovato");
        exit(-1);
    }

    printf("digitare un nome di persona:\n");
    scanf("%s", p);
    
    while(!feof(f))
    {
    fscanf(f, "%s %s %s", per.nome, per.indirizzo, per.numero);
    if (strcmp(per.nome,p)==0)
    {
        printf("%s %s %s\n", per.nome, per.indirizzo, per.numero);
        i++;
        break;
    }
    }

    if(i==0)
       printf("Persona non trovata\n");
       
    fclose(f);
}