#include <stdio.h>
#include <stdlib.h>

typedef struct {
  char nome[11];
  int stipendio;
} Persona;

int main(void) {
  FILE* f;
  Persona p;
  if ((f = fopen("stipendi.dat", "r+b")) == NULL)
    exit(-1);

  while (fread(&p, sizeof(Persona), 1, f) == 1) {
    if (p.stipendio < 1000) {
      p.stipendio += p.stipendio / 10;
      fseek(f, -sizeof(Persona), SEEK_CUR);
      fwrite(&p, sizeof(Persona), 1, f);
      fseek(f, 0, SEEK_CUR);
    }
  }
  fseek(f, 0, SEEK_SET);
  while (fread(&p, sizeof(Persona), 1, f) == 1)
    printf("%s %d\n", p.nome, p.stipendio);
  fclose(f);
  return 0;
}