#include <stdio.h>
#define DIM 5

int main(void)
{
    int n[DIM];
    int i, s = 1;
    printf("scrivere un numero:\n");
    for (i = 0; i < DIM; i++)
    {
        scanf("%d", &n[i]);
    }
    for (i = 0; i < DIM; i++)
    {
        if (i != 4)
            printf(" %d *", n[i]);
        else
            printf(" %d =", n[i]);
    }

    for (i = 0; i < DIM; i++)
    {
        s = s * n[i];
    }
    printf(" %d\n", s);

    return 0;
}
