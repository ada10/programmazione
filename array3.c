#include <stdio.h>
#define DIM 100

void leggiarray(int a[], int n)
{
    int i;
    for (i = 0; i < n; i++)

        scanf("%d", &a[i]);
}

int find(int f, int a[])
{
    int i;
    for (i = 0; a[i] != '\0'; i++)
    {
        if (f == a[i])
            return i;
    }
    return -1;
}

int main(void)
{
    int a[DIM], dl, x;

    printf("inserire la lunghezza della stringa:\n");
    scanf("%d", &dl);
    printf("inserire 1 o piu' numeri interi:\n");
    leggiarray(a, dl);
    printf("inserire un numero per trovarlo nell'array:\n");
    scanf("%d", &x);
    if (find(x, a) == -1)
        printf("Non esiste\n");
    else
        printf("%d\n", find(x, a));

    return 0;
}