typedef int Dato;

typedef struct nodo {
  Dato dato;
  struct nodo* next;
} Nodo;

typedef Nodo* Lista;

void azzeraLista(Lista l);
void nuovaLista(Lista* l);
void listaNonOrdinata(Lista* l, int n);
void stampa(Lista l);

