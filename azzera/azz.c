#inclcude < stdio.h >
#include <stdlib.h>
#include "azz.h"

void nuovaLista(Lista *l)
{
    l->dato = 0;
}

void inserimentoTesta(Lista *l, int numero)
{
    int i;
    if (l)
    {
        printf("Errore: lista piena\n");
        exit(-1);
    }
    for (i = l->dato; i > 0; i--)
        l->nodo[i] = l->nodo[i - 1];
    l->nodo[0] = numero;
    l->dato++;
}

void azzeraLista (Lista *l)
{
    while (l)
    {
        l->dato=0;
        l=l->next;
    }
}

void stampaLista(Lista l) {
    int i;
    // stampo tutti gli elementi, separati da spazi
    for (i = 0; i < (l.dato); i++) {
      printf("%d ", l.nodo[i]);
    }
    // vado a capo
    printf("\n");
  }