typedef int Dato;

typedef struct nodo{
    Dato dato;
    struct nodo *next;
} Nodo;

typedef Nodo *Lista;

void nuovalista(Lista *l);
void inserimentoTesta(Lista *l, int numero);
void azzeraLista (Lista *l);
void stampaLista(Lista *l);