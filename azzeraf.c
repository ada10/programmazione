#include <stdio.h>
#include <stdlib.h>
#include "tipi.h"

void azzeraLista(Lista l) {
  while (l != NULL) {  //Finche la lista non è vuota, si può anche scrivere while(*l)
    l->dato = 0;  //lista l punta a dato.lista, che diventa =0
    l = l->next;  //l diventa l'elemento a seguire della lista
  }
}

void nuovaLista(Lista* l) {
  *l = NULL;  //crea semplicemente una nuova lista.. ricordare <stdlib.h>
}


void listaNonOrdinata(Lista* l, int n) {
  int a[] = {6, 2, 3, 2, 4, 7, 0, 2, 5, 1}; //lista di input
  int i; //contatore
  for (i = 0; i < n; i++) { //for each
    (*l) = (Nodo*)malloc(sizeof(Nodo));  //l diventa un nodo
    (*l)->dato = a[i];  /*a quel nodo viene puntato a[i], (*l)->dato può anche essere scritto come *l.dato, ma con liste delinate da strutture piu complesse diventa un problema*/
    (*l)->next = NULL;
    l = &(*l)->next;
  }
}


#include <stdlib.h>
#include "tipi.h"
int vuota(Lista l) {
  return l == NULL; //Funzioni di controllo se non si è sicuri del corretto funzionamento di una lista
}

#include "tipi.h"
int piena(Lista l) { //come quella di sopra
  return 0;
}

//inserisce un nodo a testa
void insTesta(Lista* l, Dato d) {
  Nodo* aux = (Nodo*)malloc(sizeof(Nodo)); //Crea uno spazio di memoria per un nodo che chiama aux all'inizio della lista
  aux->dato = d; //Allo spazio chiamato aux.dato, inserisce il dato d
  aux->next = *l; //Punta al prossimo elemento
  *l = aux; //E all'elemento dopo inserisce il vecchio elemento
}

void insOrd(Lista* l, int d) { //Combina le funzioni inserimento testa, e ricerca per inserire tutto in modo ordinato. Utile quando le funzioni ricerca e testa vengono usate più volte nel programma.
  l = ricerca(l, d);
  insTesta(l, d);
}

#include "tipi.h"
Lista* ricerca(Lista* l, int d) { //questa parla da sola.
  while (*l) {
    if ((*l)->dato > d)
      break;
    l = &(*l)->next;
  }
  return l;
}

// Elim Testa e Elim 1
#include <malloc.h>
#include "elimTesta.h"
#include "tipi.h"

int elim1(Lista* l, int d) { //per eliminare un singolo elemento, si cerca la testa della lista con quell'elemento e si elimina la testa. Se si deve eliminare una serie di elementi si fa prima a includere l'eliminazione della testa nella ricerca
  l = ricerca(l, d);
  if (*l) {
    elimTesta(l);
    return 1;
  } else
    return 0;
}

void elimTesta(Lista* l) {
  Nodo* aux = *l;
  *l = (*l)->next;
  free(aux);
}
// Fine Elim Testa e Elim1

int lunghezza(Lista l) { //non serve commentare
  int n = 0;
  while (l) {
    n++;
    l = l->next
  }
  return n;
}

#include <stdio.h>
#include "tipi.h"
void stampa(Lista l) { //se non capite questa cambiate facoltà
  while (l) {
    printf("%d ", l->dato);
    l = l->next;
  }
  printf("\n");
}

int elementoMassimo(Lista l) { //Ritorna l'elemento di nome Massimo.
  int max;
  if (l == NULL) {
    exit(-1);
  }
  max = l->dato;
  l = l->next;
  while (l) {
    if (l->dato > max) {
      max = l->dato;
    }
    l = l->next;
  }
  return n;
}

void reverse(Lista l1, Lista* l2)  // visto che l2 deve essere modificato, si punta ad l2 ma non a l1.
        * l2
    == NULL;
while (l1) {
  insTesta(l2, l1->dato);
  l1 = l1->next;
}
}

#include <stdio.h>
#include "tipi.h"
int somma(Lista l) {  // Somma degli elementi in una lista
  int s = 0;
  while (l) {
    s += l->dato;
    l = l->next;
  }
  return s;
}

#include "elimTesta.h"
#include "insOrd.h"
#include "tipi.h"
void insertionSort(Lista* l1) { //modo per ordinare una lista l1 in una lista l2. Quando lo ha spiegato ha lezione non si è dilungato molto sul perchè, ma ha solamente specificato che ci può tornare utile, come il maiale, non si butta mai niente.
  Lista l2;
  nuovaLista(&l2);
  while (*l1) {
    insOrd(&l2, (*l1)->dato);
    elimTesta(l1);
  }
  *l1 = l2;
}

void elimTutti(Lista* l, int d) { 
  while (*l) {
    l = ricerca(l, d);
    if (*l)
      elimTesta(l);
  }
}

#include <malloc.h>
#include "insCoda.h"
#include "tipi.h"

void inserimentoCoda(Lista* l, int d) {
  l = ricerca(l);
  insTesta(l, d);
}

void elimTutti(Lista* l, int d) { //elimina tutti i d da una lista l
  while (*l) {
    l = ricerca(l, d);
    if (*l)
      elimTesta(l);
  }
}
