#include <stdio.h>
#define DIM 21

float funzione(float a, float b, float c, float x)
{
    float y;
    y = (a * (x * x) + (b * x) + c);

    return y;
}

int main(void)
{
    float a, b, c, x=-1.0, y;
    int i;
    float cx[DIM];
    float cy[DIM];


    printf("scrivere 3 numeri reali (a, b, c) :");
    scanf("%f%f%f", &a, &b, &c);

    for(i=0;i<DIM;i++) {
        
        cx[i]=x;
        cy[i]=funzione(a,b,c,x);
        printf("%f %f \n", cx[i], cy[i]);
        x+=0.1;        
    }


   return 0;
}

// versione semplice senza array
   /*    
   for (x = -1.0; x < 1.1; x += 0.1)
    {
        printf("%f %f \n", x, funzione(a, b, c, x));
    }
    */
    