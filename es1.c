#include <stdio.h>
 
int somma(int n)
{
    if (n<=0)
    return 0;

    else return n+somma(n-1);
}

int main (void)
{   
    int n, tot;
    printf("inserire un numero da rendere fattoriale:\n");
    scanf("%d", &n);
    tot= n+somma(n-1); 
    printf("%d\n", tot);
    return 0;
}