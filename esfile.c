#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DIM 100
int main (void)
{
    FILE *f;
    char p[DIM];

    f=fopen("prova.txt", "wt");
    if (f==NULL)
    {
        printf("prova.txt FAILED");
        exit (-1);
    }
    while (strcmp (p,"FINE")!=0)
    {
        scanf("%s", p);
        
        fprintf(f,"%s", p);
    }

    if (fclose(f)!=0)
    {
        printf("erorre chiusura file\n");
        exit (-2);
    }
    return 0;
}