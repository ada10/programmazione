#include <stdio.h>
#include <stdlib.h>

int main (void) {

    int dimensione, i;
    int* p;
    scanf("%d", &dimensione);

    p = (int*)malloc(sizeof(int)*dimensione);
    if (p== NULL)
    exit(-1);

    for (i=0;i< dimensione;i++)
        p[i]=i;

    for (i=0;i < dimensione;i++)
        printf("%d ", p[i]);
        printf("\n");

        return 0;

}
