#include <stdio.h>
#include <math.h>

#define PI 3,14

typedef enum {
    quadrato,
    cerchio,
    rettangolo,
    triangolo
} tipofigura;

typedef struct
{
    tipofigura tipo;
    union {
        struct
        {
            float lato;
        } dati_quadrato;
        struct
        {
            float raggio;
        } dati_cerchio;
        struct
        {
            float base;
            float altezza;
        } dati_rettangolo;
        struct
        {
            float lato1;
            float lato2;
            float lato3;
        } dati_triangolo;

    } dato;
} figura;

figura quadrato(float lato)
{
    figura f;
    f.tipo = quadrato;
    f.dato.dati_quadrato.lato = lato;
    return f;
}

figura cerchio (float raggio) {
    figura f;
    f.tipo=cerchio;
    f.dato.dati_cerchio.raggio=raggio;
    return f;
}

figura rettangolo (float base, float altezza) {
    figura f;
    f.tipo=rettangolo;
    f.dato.dati_rettangolo.base=base;
    f.dato.dati_rettangolo.altezza=altezza;
    return f;
}

figura triangolo (float lato1, float lato2, float lato3) {
    figura f;
    f.tipo=triangolo;
    f.dato.dati_triangolo.lato1=lato1;
    f.dato.dati_triangolo.lato2=lato2;
    f.dato.dati_triangolo.lato3=lato3;
    return f;
}


float perimetro (figura f) {
    switch (f.tipo) {
        case triangolo:
        return f.dato.dati_triangolo.lato1+f.dato.dati_triangolo.lato2
        +
                f.dato.dati_triangolo.lato3;
    }
}

float area(figura f)
{
    switch (f.tipo)
    {
        case quadrato:
        return f.dato.dati_quadrato.lato*f.dato.dati_quadrato.lato;
        case cerchio:
        return PI*f.dato.dati_cerchio.raggio*f.dato.dati_cerchio.raggio;
        case rettangolo:
        return f.dato.dati_rettangolo.base*f.dato.dati_rettangolo.altezza;
        case triangolo:
        float p=//funzione perimetro/2
        return (f.dato.dati_triangolo.*f.dato.dati_triangolo.)/2;
    }
}

int main(void)
{
    figura f;
    f = quadrato(5.0);
    printf("%f\n", area (f));
    // f=cerchio(10.0);
    //printf("%f\n", area(f));
    //f=rettangolo(5.0, 10.0)
    //printf("")
}

