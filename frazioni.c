#include <stdio.h>
typedef struct {
    int num;
    int den;
    }   Frazione;

void leggiFrazione (Frazione*f) {
    scanf("%d%d", &(*f).num, &(*f).den); 
}                                   

void stampaFrazione(Frazione f) {
    printf("%d/%d", f.num, f.den);

}

int main(void) {
    Frazione f;
    leggiFrazione(&f);
    stampaFrazione(f);
    printf("\n");
    return 0;
}
