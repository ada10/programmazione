#include <stdlib.h>
#include "tipi.h"

int rnd_int(int min, int max) {
  return min + rand() % (max - min + 1);
}

void listaNonOrdinata(Lista* l, int n) {
  int a[] = {6, 2, 3, 2, 4, 7, 0, 2, 5, 1};
  int i;
  for (i = 0; i < n; i++) {
    (*l) = (Nodo*)malloc(sizeof(Nodo));
    (*l)->dato = a[i];
    (*l)->next = NULL;
    l = &(*l)->next;
  }
}

void listaOrdinata(Lista* l, int n) {
  int a[] = {2, 3, 4, 5, 7, 8, 12, 15, 21, 24};
  int i;
  for (i = 0; i < n; i++) {
    (*l) = (Nodo*)malloc(sizeof(Nodo));
    (*l)->dato = a[i];
    (*l)->next = NULL;
    l = &(*l)->next;
  }
}