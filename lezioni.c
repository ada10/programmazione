
#include <stdio.h>

typedef struct
{
    char nomecorso[20];
    int orainizio, durata;
} orario;

void verifica(int *ora1, int *ora2, int durata1, int durata2)
{
    int i, j;
    if (*ora1 == *ora2)

        *ora2 += durata1;

    else if (*ora1 < *ora2)
    {

        j = durata1;
        for (i = 0; i < j; i++)
        {
            if (*ora1 == *ora2)
            {
                *ora2 += durata1;
                *ora1 -= i;
                break;
            }
            else
                (*ora1)++;
            durata1--;
        }
    }

    else if (*ora1 < *ora2)
    {

        j = durata2;
        for (i = 0; i < j; i++)
        {
            if (*ora2 == *ora1)
            {
                *ora1 += durata2;
                *ora2 -= i;
                break;
            }
            else
                (*ora2)++;
            durata2--;
        }
    }
}

int main(void)
{
    orario orario1, orario2;
    int i;

    printf("inserire 2 lezioni(inizio durata lezione):\n");
    scanf("%d %d %s", &orario1.orainizio, &orario1.durata, orario1.nomecorso);
    scanf("%d %d %s", &orario2.orainizio, &orario2.durata, orario2.nomecorso);

    verifica(&orario1.orainizio, &orario2.orainizio, orario1.durata, orario2.durata);

    printf("%s si svolgera' alle %d e durera' %d ore\n", orario1.nomecorso, orario1.orainizio, orario1.durata);
    printf("successivamente si svolgera' %s alle %d e durera' %d ore\n", orario2.nomecorso, orario2.orainizio, orario2.durata);

    return 0;
}