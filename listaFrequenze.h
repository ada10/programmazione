#define LUNGHEZZA_MAX 30    

typedef struct
{
    char parola[LUNGHEZZA_MAX];
    int n_occorrenze;
} Frequenza;

typedef struct nodo {
  Frequenza dato;
  struct nodo* next;
} Nodo;

typedef Nodo* Lista;

void nuovaLista(Lista *l);
void aggiornamentoFrequenze(Lista *l, char parola[]);
void stampaFrequenze (Lista l, char parola[]);
