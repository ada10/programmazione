#include <stdio.h>
#include <stdlib.h>
#define DIMENSIONE 50


typedef struct
{
    int n_elementi;
    int dati[DIMENSIONE];
} Lista;


void nuova_lista(Lista *l);

void inserimento_ordinato(Lista *l, int numero);

void stampa(Lista l);