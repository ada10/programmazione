#include "listaFrequenze.h"
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) 
{
    Lista l;
    FILE *f_in;
    char parola[LUNGHEZZA_MAX];
    
    if (argc !=3)
    {
        printf("Uso: %s file_in file_out\n", argv[0]);
        exit (-1);
    }

    nuovaLista(&l);

    if ((f_in= fopen(argv[1], "rt"))=NULL)
    {
        printf("errore apertura file %s\n", argv[1]);
        exit(-2);
    }
    while (fscanf(f_in, "%s", parola)==1)
        aggiornamentoFrequenze(&l, parola);
    
    
    
    
    return 0;
}

