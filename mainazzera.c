#include <stdio.h>
#include <stdlib.h>


typedef struct nodo {
  int dato;
  struct nodo* next;
} Nodo;

typedef Nodo* Lista;



void nuovaLista(Lista* l)
{
    *l=NULL;
}

void listaNonOrdinata(Lista* l)
{
  int a[] = {6, 2, 3, 2, 4, 7}; //lista di input
  int i; //contatore
  for (i = 0; i < 6; i++) { //for each
    (*l) = (Nodo*)malloc(sizeof(Nodo));  //l diventa un nodo
    (*l)->dato = a[i];  /*a quel nodo viene puntato a[i], (*l)->dato può anche essere scritto come *l.dato, ma con liste delinate da strutture piu complesse diventa un problema*/
    (*l)->next = NULL;
    l = &(*l)->next;
  }
}

void azzeraLista(Lista l)
{
  while (l) {  //Finche la lista non è vuota, si può anche scrivere while(*l)
    l->dato = 0;  //lista l punta a dato.lista, che diventa =0
    l = l->next;  //l diventa l'elemento a seguire della lista
  }
}

void stampa(Lista l)
{ //se non capite questa cambiate facoltà
  while (l) {
    printf("%d ", l->dato);
    l = l->next;
  }
  printf("\n");
}


int main (void)
{
    Lista *l;
    nuovaLista(&l);
    listaNonOrdinata(&l);
    azzeraLista(  l);
    stampa(l);
    
    return 0;
}