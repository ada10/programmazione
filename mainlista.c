#include <stdio.h>
#include <stdlib.h>
#include "listaordinata.h"
#define DIM 50


int main (void)
{
    int numero;
    Lista l;
    
    printf("inserire MAX 50 numeri POSITIVI (per terminare inserire un numero <=0):\n");
    nuova_lista(&l);
    
    do 
    {
        scanf("%d", &numero);
        if (numero >0)
            inserimento_ordinato(&l, numero);
    }

    while (numero > 0)

    stampa(l);
    
    return 0;
}