#include <stdio.h>

int massimo (int a,int b) {
    if (a>=b)
        return a;
    else 
        return b;
            
}

int max3 (int a, int b, int c) {
    if (massimo(a,b)>=c)
        return massimo(a,b);
    else 
        return c;
            
}

int main(void) {
    int x, y, z;
    printf("scrivere tre numeri:\n");
    scanf("%d %d %d", &x, &y, &z);
    printf("il valore massimo e': %d\n", max3 (x,y,z));
    return 0;
}