#include <stdlib.h>
typedef int Dato;

typedef struct nodo {
  Dato dato;
  struct nodo* next;
} Nodo;

typedef Nodo* Lista;

Lista* ricerca(Lista *l, int d) {
  while (*l) 
    l = &(*l)->next;
  return l;
}

void insCoda(Lista* l, int d) {
  l = ricerca(l, d);
  insTesta(l, d);
}

void insTesta(Lista *l, int numero)
{
    int i;
    if (l)
    {
        printf("Errore: lista piena\n");
        exit(-1);
    }
    for (i = l->dato; i > 0; i--)
        l->nodo[i] = l->nodo[i - 1];
    l->nodo[0] = numero;
    l->dato++;
}



void azzeraLista(Lista l) {
  while (l != NULL) {
    l->dato = 0;
    l = l->next;
  }
}

int main() {
  Lista l;
  insCoda(&l, 6);
  azzeraLista(l);
}