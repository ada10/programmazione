#include <stdio.h>
#include <stdlib.h>

typedef struct {
  char cognome[31];
  char nome[31];
  char sesso;
  int anno;
} Persona;

Persona leggiPersona() {
Persona p;
  char s[2];
  printf("Cognome\n");
  scanf("%s", p.cognome);
  printf("Nome?\n");
  scanf("%s", p.nome);
  printf("Sesso\n");
  scanf("%s", s);
  p.sesso = s[0];
  printf("Anno di nascita?\n");
  scanf("%d", &p.anno);
  return p;
}

int main() {
  FILE* f;
  Persona p;
  char risposta[2];

  if ((f = fopen("people.dat", "wb")) == NULL)
    exit(-1);

  do {
    p = leggiPersona();
    fwrite(&p, sizeof(Persona), 1, f);
    printf("Altra persona (s/n)?\n");
    scanf("%s", risposta);
  } while (risposta[0] == 's');
  fclose(f);
}
