#include <stdio.h>

int primo (int n) {
    int i,j=0;
    for (i=1;i<=n;i++) {
        if (n%i==0)
        j++;
    }
    if (j==2)
        return 1;
    else
        return 0;  
}

int main (void) {
    int n, i;
    printf("inserire un numero:\n");
    scanf("%d", &n);
    for(i=2;i<=n;i++) {
        if (primo(i))
            printf("%d\n", i);
          
    }

    return 0;
}