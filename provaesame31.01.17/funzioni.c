#include <stdio.h>
#include <stdlib.h>
#include "funzioni.h"

void nuovaLista(Lista *l)
{
    *l = NULL;
}

void inserimentoNonOrdinato(Lista *l, Dato d)
{

    Nodo *aux;
    
        aux = malloc(sizeof(Nodo));
        aux->dato = d;
        aux->next = *l;
        *l = aux;
    
}

void stampaLista(Lista *l, Dato d, int y)
{
        FILE *f;
        
        
        if ((f = fopen("clientirecenti.dat", "wb")) == 0)
        {
            printf("Impossibile aprire il file %s\n", argv[2]);
            exit(-3);
        }

        while(fread(&d, sizeof(Dato), 1, f) == 1)
        {
            if (d.anno >= y)
            {
                fwrite(&d, sizeof(Dato), 1, f);
            }
            
        }
        fclose(f);
}

void insOrd(Lista* l, Dato d) { //Combina le funzioni inserimento testa, e ricerca per inserire tutto in modo ordinato. Utile quando le funzioni ricerca e testa vengono usate più volte nel programma.
  while (*l) {
    if ((*l)->dato > d)
      break;
    l = &(*l)->next;
  }
  Nodo* aux = (Nodo*)malloc(sizeof(Nodo)); //Crea uno spazio di memoria per un nodo che chiama aux all'inizio della lista
  aux->dato = d; //Allo spazio chiamato aux.dato, inserisce il dato d
  aux->next = *l; //Punta al prossimo elemento
  *l = aux;
}

void insertionSort(Lista* l){ //modo per ordinare una lista l1 in una lista l2. Quando lo ha spiegato ha lezione non si è dilungato molto sul perchè, ma ha solamente specificato che ci può tornare utile, come il maiale, non si butta mai niente.
  Lista l2;
  nuovaLista(&l2);
  while (*l) {
    insOrd(&l2, (*l)->dato);
    Nodo* aux = *l;
  *l = (*l)->next;
  free(aux);
  }
  *l = l2;
}

void stampaTesto(Lista l, char nomeFile[], int y)
{
    FILE *f;  
    
    if((f=fopen(nomeFile,"wt"))==0)
    {
        printf("Impossibilie aprire il file %s\n", nomeFile);
        exit(-4);
    }

    while(l)
    fprintf(f,"%d %s\n", l->dato.anno, l->dato.nome);
    l=l->next;
    fclose(f);

}





