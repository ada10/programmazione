typedef struct
{
    char nome[30];
    int anno;
} Dato;

typedef struct nodo {
    Dato dato;
    struct nodo* next;
} Nodo;
  
  typedef Nodo* Lista;


void nuovaLista(Lista *l);
void inserimentoNonOrdinato(Lista* l, Dato d);
void insertionSort(Lista* l);
void insOrd(Lista* l, int d);
void stampaLista(Lista *l, Dato d, int y);
void stampaTesto(Lista l, char nomeFile[], int y);