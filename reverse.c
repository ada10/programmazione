#include <stdio.h>
#include "tipi.h"

void listanonordinata(Lista* l, int n);

void insTesta(Lista* l, Dato d)
{
    Nodo* aux=(Nodo*)malloc(sizeof(Nodo));
    aux->dato=d;
    aux->next=*l;
    *l=aux;

}

void reverse(Lista* l1, Lista* l2)
{
    *l2=NULL;
    while(l1) 
    {
        insTesta(l2, l1->dato);
        l1=l1->next;
    }

}
int main (void)
{
    Lista l1, l2;
    listanonordinata(&l1,4);
    reverse(l1, &l2);
    

    return 0;
}