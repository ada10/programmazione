typedef struct
{
    char nome[50];
    char via[50];
    int numero;
} persona;

int ricerca(persona indirizzi[], char nome[50], int first, int last)
{
    if (first > last)
        return -1;
    else
    {
        int med = (first + last) / 2;
        if (strcmp(nome, indirizzi[med].nome) == 0)
            return med;
        else if (strcmp(nome, indirizzi[med].nome) < 0)
            ricerca(indirizzi[], nome, first, med - 1);
        else
            ricerca(indirizzi, nome, med + 1, last);
    }
}