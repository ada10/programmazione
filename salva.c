#include <stdio.h>
#include <stdlib.h>
#define DIM 41

int main (void) {

    FILE *f;
    char s[DIM];
    f=fopen ("prova.txt", "wt");
    if (f==NULL)
    {
        printf("impossibile aprire il file\n");
        exit(-1);
    }
    do {
    scanf("%s", s);
    fprintf(f, "%s\n", s);
    } while (strcmp(s,"FINE") !=0);

    if (fclose(f) !=0) {
        printf("impossibile chiudere il file\n");
        exit (-2);
    }
    return 0;
}