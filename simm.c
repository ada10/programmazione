#include <stdio.h>

void lettura(int a[3][3])
{
    int i, j;
    for (i=0;i<3;i++)
            for (j=0;j<3;j++)
            {
                printf("inserire un numero:\n");
                scanf("%d", &a[i][j]);
            }
            printf("\n");
}

int simm (int a[3][3])
{
    int i, j;
    for (i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            if (a[i][j]!=a[j][i])
            return -1;
        }
    }
    return 0;
}

int main (void)
{
    int m [3][3];
    int i, j;
    lettura(m);
    if (simm(m)==-1)
        printf("non e' simmetrica\n");
    else
        printf("e' simmetrica\n");
        for (i=0;i<3;i++)
        {
            for (j=0;j<3;j++)
            {
                printf("%3d", m[i][j]);
            }
            printf("\n");
        }
    return 0;
}