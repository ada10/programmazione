#include <stdio.h>
#define DIM 200 

int a[DIM], b[DIM/4];

// funzione bisestile
int bisestile (int ab) {
    
    if((ab%4==0 || ab%100==0) && (ab%400!=0))
    
            return 1;
                else 
                    return 2;
        
}

int main(void) {
    
    int i, m=0;
    for (i=0;i<=DIM; i++)
    a[i]=1900+1;
    for (i=0;i<=DIM; i++)
    if(bisestile(a[i])==1) {
        b[m]=a[i];
        m++;
    }
    i=0;
    while(b[i]!='\0') {
        printf("%d",b[i]);
        i++;
    }
    printf("\n");
    
    return 0;
}