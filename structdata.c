#include <stdio.h>

int main(void)
{
    struct data
    {
        int giorno, mese, anno;
    } d1, d2;

    int dif;

    printf("inserire una data (GG MM AAAA) :");
    scanf("%d %d %d", &d1.giorno, &d1.mese, &d1.anno);
    printf("inserire una data (GG MM AAAA) :");
    scanf("%d %d %d", &d2.giorno, &d2.mese, &d2.anno);

    if (d1.anno < d2.anno)
        dif = 1;
    if (d1.anno > d2.anno)
        dif = -1;
    if (d1.anno == d2.anno)
    {
        if (d1.mese < d2.mese)
            dif = 1;
        if (d1.mese > d2.mese)
            dif = -1;
        if (d1.mese == d2.mese)
        {
            if (d1.giorno < d2.giorno)
                dif = 1;
            if (d1.giorno > d2.giorno)
                dif = -1;
            if (d1.giorno == d2.giorno)
                dif = 0;
        }
    }

    if (dif == 0)
        printf("le date sono uguali\n");
    if (dif < 0)
        printf("la seconda data precede la prima\n");
    if (dif > 0)
        printf("la prima data precede la seconda\n");
    return 0;
}