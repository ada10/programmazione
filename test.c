#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *fp;
    int numero;

    fp = fopen("prove.txt", "rt");
    if (fp == NULL)
    {
        PRINTF("Errore apertura file prove.txt\n");
        exit(-1);
    }

    while (fscanf(fp, "%d", &numero) != EOF)
    {
        PRINTF("%d", numero);
    }

    fclose(fp);

    return 0;
}