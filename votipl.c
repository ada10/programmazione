#include <stdio.h>
#include <stdlib.h>
#define DIM 30
#define DIM2 5

typedef struct
{
    char nome[DIM];
    int matricola, voto;
} studente;

int main(void)
{
    studente std[DIM2];
    int i;
    srand(time(NULL));

    for (i = 0; i < 5; i++)
    {
        printf("digitare Nome e Matricola dello studente: ");
        scanf(" %s %d", std[i].nome, &std[i].matricola);
    }

    for (i = 0; i < DIM2; i++)
    {
        std[i].voto = (rand() % 31);
        if (std[i].voto >= 18)
            printf("%10s %10d %10d\n", std[i].nome, std[i].matricola, std[i].voto);
    }

    return 0;
}