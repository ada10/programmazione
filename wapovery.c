#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#define DIM 2048

int main(int argc, char **argv){
	int err, sd;
	struct addrinfo hints, *ptr, *res;

	if(argc != 3){
		printf("SintassI: ./nomefile <hostname> <porta>\n");
		exit(1);
	}

	memset(&hints, 0, sizeof(hints));
	hints.ai_family=AF_UNSPEC;
	hints.ai_socktype=SOCK_STREAM;

	if((err=getaddrinfo(argv[1], argv[2], &hints, &res))!=0){
		fprintf(stderr, "Errore risoluzione nome: %s\n", gai_strerror(err));
		exit(2);
	}
	for(ptr=res; ptr!=NULL; ptr=ptr->ai_next){
		if((sd=socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol))<0){
			fprintf(stderr, "Errore creazione socket\n");
			continue;
		}
		if(connect(sd, ptr->ai_addr, ptr->ai_addrlen)==0){
			break;
		}
		close(sd);
	}
	if(ptr==NULL){
		fprintf(stderr, "Errore risoluzione nome\n");
		exit(3);
	}
	freeaddrinfo(res);
//--------------------------------
	char nome[DIM], mess[DIM],buff[DIM], destinatario[DIM];
	char *n;
	
	write(1, "Inserire proprio nome: ", 24);
	memset(nome, 0, sizeof(nome));
	read(0, nome, sizeof(nome));
	write(sd, nome, sizeof(nome));
	memset(buff, 0, sizeof(buff));
	read(sd, buff, sizeof(buff));
	if(strcmp(buff, "nessun messaggio")!=0){
		write(1, buff, sizeof(buff));
	}
	n=strtok(nome, "\n");
	while(1){
		memset(destinatario, 0, sizeof(destinatario));
		write(1, "Inserire destinatario: ", 24);
		read(0, destinatario, sizeof(destinatario));
		memset(buff, 0, sizeof(buff));
		strcat(buff, n);
		strcat(buff, ": ");
		memset(mess, 0, sizeof(mess));
		write(1, "Inserire testo: ", 17);
		read(0, mess, sizeof(mess));
		strcat(buff, mess);
		write(sd, destinatario, sizeof(destinatario));
		write(sd, buff, sizeof(buff));

		memset(buff, 0, sizeof(buff));
		read(sd, buff, sizeof(buff));
		write(1, buff, sizeof(buff));
	}
//--------------------------------
	close(sd);

	return 0;
}